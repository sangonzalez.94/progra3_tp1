package progra3_tp1.modelos;

public class ParFilaColumna {
	private int fila = -1;
	private int columna = -1;
	private boolean disponible = false;
	
	public ParFilaColumna (int fila, int columna) {
		this.fila = fila;
		this.columna = columna;
	}

	public int getFila() {
		return fila;
	}

	public void setFila(int fila) {
		this.fila = fila;
	}

	public int getColumna() {
		return columna;
	}

	public void setColumna(int columna) {
		this.columna = columna;
	}

	public boolean isDisponible() {
		return disponible;
	}

	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}

	@Override
	public boolean equals(Object obj) {
		ParFilaColumna parRecibido = (ParFilaColumna) obj;
		if(parRecibido.getColumna() == this.getColumna() && parRecibido.getFila() == this.getFila()) {
			return true;
		}else {
			return false;
		}
	}
	
	
	
}
