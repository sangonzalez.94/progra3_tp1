package progra3_tp1.modelos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import progra3_tp1.utilidades.excepciones.SinParDeNumerosExcepcion;

import static progra3_tp1.utilidades.NumerosUtil.numeroAlAzar;

public class Tablero {
	private boolean gano = false;
	private int[][] matriz;
	private int longitudFila = 4;
	private int longitudColumna = 4;
	private List<ParFilaColumna> listaParFilaColumna;
	boolean tableroCompleto = false;
	public Tablero(int[][] tablero) {
		this.matriz = tablero;
		listaParFilaColumna = new ArrayList<>();
		cargarListaDePares();
	}

	public Tablero(int longitudFila, int longitudColumna) throws SinParDeNumerosExcepcion {
		iniciarTablero(longitudFila,longitudColumna );
	}

	// TODO
	public void iniciarTablero(int longitudFila, int longitudColumna) throws SinParDeNumerosExcepcion {
		this.longitudFila = longitudFila;
		this.longitudColumna = longitudColumna;
		matriz = new int[longitudFila][longitudColumna];
		
		cargarListaDePares();
		nuevoNumeroEnTablero();
		nuevoNumeroEnTablero();
	}
	
	public void nuevoNumeroEnTablero() {
		//Busco una posici�n al azar para poner un nuevo n�mero.
		int fila = numeroAlAzar(0, longitudFila-1);
		int columna = numeroAlAzar(0, longitudColumna-1);
		int nroNuevo = numeroAlAzar(2, 4, true);
		if(matriz [fila] [columna] != 0 && !tableroCompleto) {
			nuevoNumeroEnTablero();
		}else if (matriz[fila] [columna] == 0) {
			matriz[fila][columna] = nroNuevo;
		}
		actualizarEstadoTablero();
	}

	private void actualizarEstadoTablero() {
		int cantidadCeros = longitudFila * longitudColumna;
		for(int fila = 0; fila < longitudFila ; fila ++) {
			for (int columna = 0; columna < longitudColumna; columna ++) {
				if(matriz[fila][columna]>0) {
					cantidadCeros--;
				}
			}
		}
		if (cantidadCeros <= 0)
			tableroCompleto = true;
	}
	
	
	private void cargarListaDePares() {
		listaParFilaColumna = new ArrayList<>();
		for(int fila = 0; fila < longitudFila ; fila ++) {
			for (int columna = 0; columna < longitudColumna; columna ++) {
				
					ParFilaColumna par = new ParFilaColumna(fila, columna);
					listaParFilaColumna.add(par);
				
			}
		}
	}

	/*public void nuevoNumeroEnTablero() throws SinParDeNumerosExcepcion {
		//Busco una posici�n al azar para poner un nuevo n�mero.
		int nroNuevo = numeroAlAzar(2, 4, true);
		Collections.shuffle(listaParFilaColumna);
		boolean parEncontrado = false;
		ParFilaColumna parfc = null;
		for(ParFilaColumna  parfilacolumna: listaParFilaColumna) {
			if(!parfilacolumna.isDisponible() && !parEncontrado) {
				parfc = parfilacolumna;
			}
		}

		matriz[parfc.getFila()][parfc.getColumna()] = nroNuevo;
		parfc.setDisponible(false);
	
	}

	*/
	
	/**
	 * Suma y mueve los numeros hacia la izquierda.
	 * Condiciones:
	 * 1) si hay 4 numeros iguales, debe sumar de a 2.
	 * Por ej: 2 | 2 | 2 | 2 | => 4 | 4 | 0 | 0
	 * 2) si hay un solo n�mero. Este se dirige completamente hac�a la izquierda.
	 * 3) si son todos distintos, ocupando todos los casilleros, queda igual.
	 * 4) si son dos numeros distintos, no se suman. Y se mueven hacia la izquierda.
	 * @return
	 * @throws SinParDeNumerosExcepcion 
	 */
	public int[][] accionIzquierda() throws SinParDeNumerosExcepcion {
		sumarHaciaLaIzquierda();
		reubicarNumerosHaciaLaIzquierda();
		nuevoNumeroEnTablero();
		return matriz;
	}

	/**
	 * Suma y mueve los numeros hacia la derecha.
	 * Condiciones:
	 * 1) si hay 4 numeros iguales, debe sumar de a 2.
	 * Por ej: 2 | 2 | 2 | 2 | => 0 | 0 | 4 | 4
	 * 2) si hay un solo n�mero. Este se dirige completamente hac�a la derecha.
	 * 3) si son todos distintos, ocupando todos los casilleros, queda igual.
	 * 4) si son dos numeros distintos, no se suman. Y se mueven hacia la derecha.
	 * @return
	 * @throws SinParDeNumerosExcepcion 
	 */
	public int[][] accionDerecha() throws SinParDeNumerosExcepcion {
		sumarHaciaLaDerecha();
		reubicarNumerosHaciaLaDerecha();
		nuevoNumeroEnTablero();
		return matriz;
	}

	/**
	 * Suma y mueve los numeros hacia arriba.
	 * Condiciones:
	 * 1) si hay 4 numeros iguales, debe sumar de a 2.
	 * 2) si hay un solo n�mero. Este se dirige completamente hac�a la arriba.
	 * 3) si son todos distintos, ocupando todos los casilleros, queda igual.
	 * 4) si son dos numeros distintos, no se suman. Y se mueven hacia la arriba..
	 * @return
	 * @throws SinParDeNumerosExcepcion 
	 */
	public int[][] accionArriba() throws SinParDeNumerosExcepcion {
		//Se invierte la matriz para poder utilizar el metodo accionIzquierda.
		matriz = invertirMatriz();
		this.accionIzquierda();
		// Se vuelve a invertir la matriz para volver a la normalidad.
		matriz = invertirMatriz();
		nuevoNumeroEnTablero();
		return matriz;
	}

	/**
	 * Suma y mueve los numeros hacia abajo.
	 * Condiciones:
	 * 1) si hay 4 numeros iguales, debe sumar de a 2.
	 * 2) si hay un solo n�mero. Este se dirige completamente hac�a la abajo.
	 * 3) si son todos distintos, ocupando todos los casilleros, queda igual.
	 * 4) si son dos numeros distintos, no se suman. Y se mueven hacia la arriba..
	 * @return
	 * @throws SinParDeNumerosExcepcion 
	 */
	public int[][] accionAbajo() throws SinParDeNumerosExcepcion {
		//Se invierte la matriz para poder utilizar el metodo accionDerecha.
		matriz = invertirMatriz();
		this.accionDerecha();
		// Se vuelve a invertir la matriz para volver a la normalidad.
		matriz = invertirMatriz();
		nuevoNumeroEnTablero();
		return matriz;
	}

	/**
	 * Invierte la matriz. Por lo que matriz[fila][columna] pasa a ser matriz[columna][fila]
	 * @return la matriz invertida.
	 */
	private int[][] invertirMatriz() {
		int[][] matrizT = new int[matriz.length][matriz.length];
		for (int x = 0; x < matriz.length; x++) {
			for (int y = 0; y < matriz[x].length; y++) {
				matrizT[y][x] = matriz[x][y];
			}
		}
		return matrizT;
	}

	/**
	 * Reubica los n�meros hac�a la derecha, sin sumar.
	 */
	private void reubicarNumerosHaciaLaDerecha() {
		// segundo paso, reubico los numeros.
		for (int fila = 0; fila < matriz.length; fila++) {
			int[] columnas = matriz[fila];
			for (int columna = 1; columna < matriz[fila].length; columna++) {
				if (columnas[columna - 1] > 0 && columnas[columna] == 0) {
					columnas[columna] = columnas[columna - 1];
					columnas[columna - 1] = 0;
				}
			}
			matriz[fila] = columnas;
		}
	}

	/**
	 * Suma y mueve los numeros hac�a la derecha.
	 * En caso que los n�meros se repitan, solo los suma una vez.
	 */
	private void sumarHaciaLaDerecha() {
		// Primer paso, sumo y muevo los numeros.
		for (int fila = 0; fila < matriz.length; fila++) {
			int[] columnas = matriz[fila];
			int ultimaPosicionSumada = -1;
			for (int columna = 1; columna < matriz[fila].length; columna++) {
				if (columnas[columna - 1] > 0 && columnas[columna] == 0) {
					columnas[columna] = columnas[columna - 1];
					columnas[columna - 1] = 0;
				}
				if (columnas[columna - 1] == columnas[columna] && columnas[columna - 1] != 0) {
					if (ultimaPosicionSumada != columna - 1) {
						columnas[columna] = columnas[columna - 1] * 2;
						columnas[columna - 1] = 0;
						ultimaPosicionSumada = columna;
					}

				}
			}
			matriz[fila] = columnas;
		}
	}

	/**
	 * Reubica los n�meros hac�a la izquierda
	 */
	private void reubicarNumerosHaciaLaIzquierda() {
		for (int fila = 0; fila < matriz.length; fila++) {
			int[] columnas = matriz[fila];
			for (int columna = matriz[fila].length - 2; columna >= 0; columna--) {
				if (columnas[columna + 1] > 0 && columnas[columna] == 0) {
					columnas[columna] = columnas[columna + 1];
					columnas[columna + 1] = 0;
				}
			}
			
			matriz[fila] = columnas;
		}
	}

	/**
	 * Mueve los numeros hacia la izquierda-
	 * en caso de existir coincidencia, tambien los suma.
	 * La suma la hace una unica vez. Evitando repetir la suma de numeros.
	 */
	private void sumarHaciaLaIzquierda() {
		for (int fila = 0; fila < matriz.length; fila++) {
			int[] columnas = matriz[fila];
			int ultimaPosicionSumada = -1;
			for (int columna = matriz[fila].length - 2; columna >= 0; columna--) {
				if (columnas[columna + 1] > 0 && columnas[columna] == 0) {
					columnas[columna] = columnas[columna + 1];
					columnas[columna + 1] = 0;
				}
				if (columnas[columna + 1] == columnas[columna] && columnas[columna] != 0) {
					if (ultimaPosicionSumada != columna + 1) {
						columnas[columna] = columnas[columna] * 2;
						columnas[columna + 1] = 0;
						ultimaPosicionSumada = columna;
					}

				}
			}
			matriz[fila] = columnas;
		}
	}
	
	public int[][] getTablero(){
		return matriz;
	}
	
	public boolean isTableroCompleto() {
		return tableroCompleto;
	}
}
