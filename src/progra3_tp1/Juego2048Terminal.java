package progra3_tp1;

import java.util.Scanner;

public class Juego2048Terminal {
	static int[][] matriz;

	public static void main(String args[]) {
		matriz = new int[4][4];
		// imprimirPanel();
		jugar();
	}

	private static void jugar() {
		boolean fin = false;
		iniciarTablero();

		do {
			imprimirPanel();
			String entradaTeclado = "";
			Scanner entradaEscaner = new Scanner(System.in); // Creaci�n de un objeto Scanner
			entradaTeclado = entradaEscaner.nextLine(); // Invocamos un m�todo sobre un objeto Scanner
			switch(entradaTeclado) {
			case "W": //arriba
				break;
			case "A": //izquierda
				break;
			case "D": //derecha
				accionDerecha();
				break;
			case "S"://abajo
				break;
			case "F":
				fin = true;
			}
		} while (!fin && tableroEspaciosDisponibles());
	}

	private static void accionDerecha() {
		for(int fila = 0; fila < 4; fila++) {
			int posicion = -1;
			int valor = -1;
			for (int columna = 0; columna < 4; columna++) {
				if(posicion >= 0 ) {
					if(matriz[fila][columna]==valor) {
						matriz[fila][posicion] = 0;
						matriz[fila][columna]=valor*2;
					}
					if(matriz[fila][columna]==0) {
						matriz[fila][posicion] = 0;
						matriz[fila][columna]=valor;
					}
				}
				if(matriz[fila][columna] > 0) {
					posicion = columna;
					valor = matriz[fila][columna];
				}
				
			}
			
		}
		nuevoNumeroEnTablero();
	}

	public static void nuevoNumeroEnTablero() {
		// Busco una posici�n al azar para poner un nuevo n�mero.
		if(tableroEspaciosDisponibles()) {
		int fila = numeroAlAzar(0, 4 - 1);
		int columna = numeroAlAzar(0, 4 - 1);
		int nroNuevo = numeroAlAzar(2, 4, true);
		if (matriz[fila][columna] != 0) {
			nuevoNumeroEnTablero();
		}
		// ubico el n�mero en la grilla generada.
		matriz[fila][columna] = nroNuevo;
		}
		// actualizarEstadoTablero();
	}

	private static boolean tableroEspaciosDisponibles() {
		int contador = 16;
		for(int i = 0; i < 4; i++) {
			for (int e = 0; e < 4; e++) {
				if(matriz[i][e]>0) {
					contador--;
				}
			}
		}
		return contador >0;
	}

	public static void iniciarTablero() {
		// Pongo en cero todos los casilleros.
		for (int fila = 0; fila < 4; fila++) {
			for (int columna = 0; columna < 4; columna++) {
				matriz[fila][columna] = 0;
			}
		}
		// Agrego dos numeros al azar en el tablero en posiciones aleatorias.
		nuevoNumeroEnTablero();
		nuevoNumeroEnTablero();
	}

	private static void imprimirPanel() {
		for (int fila = 0; fila < 4; fila++) {
			String filaToString = "|";
			for (int columna = 0; columna < 4; columna++) {
				String caracter = "_";
				if (matriz[fila][columna] > 0) {
					filaToString = filaToString + "|"+Integer.toString(matriz[fila][columna])+"|";
				} else {
					filaToString = filaToString + "_|";
				}

			}

			System.out.println(filaToString);
		}

	}

	/**
	 * Devuelve un numero al azar entre un numero minimo -inclusive- y un numero
	 * maximo - inclusive- con la opci�n de devolver solo n�meros pares.
	 * 
	 * @param numeroMinimo minimo tolerado
	 * @param numeroMaximo m�ximo tolerado
	 * @param par          condicion de ser o no par.
	 * @return el numero al azar.
	 */
	public static Integer numeroAlAzar(int numeroMinimo, int numeroMaximo, boolean par) {
		if (par) {
			boolean parEncontrado = false;
			int numero = 0;
			while (!parEncontrado) {
				numero = numeroAlAzar(numeroMinimo, numeroMaximo);
				if (numero % 2 == 0) {
					parEncontrado = true;
				}
			}
			return numero;
		} else {
			return numeroAlAzar(numeroMinimo, numeroMaximo);
		}
	}

	/**
	 * Devuelve un numero al azar entre un numero minimo -inclusive- y un numero
	 * maximo - inclusive-
	 * 
	 * @param numeroMinimo minimo tolerado
	 * @param numeroMaximo m�ximo tolerado
	 * @return el numero al azar.
	 */
	public static Integer numeroAlAzar(int numeroMinimo, int numeroMaximo) {
		int valorEntero = (int) Math.floor(Math.random() * (numeroMaximo - numeroMinimo + 1) + numeroMinimo);
		return valorEntero;
	}
}
