package progra3_tp1.utilidades.excepciones;

/**
 * excepcion personalizada para caso de Fila o Columna inexistente.
 * @author santiago
 *
 */
public class FilaColumnaInexistenteException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public FilaColumnaInexistenteException(String mensaje) {
		super(mensaje);
	}

}
