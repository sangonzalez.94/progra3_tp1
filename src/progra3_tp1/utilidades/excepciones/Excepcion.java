package progra3_tp1.utilidades.excepciones;

/**
 * Enum con excepciones de la aplicacion.
 * @author santiago
 *
 */
public enum Excepcion {
	COLUMNA_FILA_INEXISTENTES ("La combinación de Fila - Columna es inexistente");
	
	String mensaje;
	Excepcion (String mensaje) {
		this.mensaje = mensaje;
	}
	
	public String getMensaje() {
		return this.mensaje;
	}
}
