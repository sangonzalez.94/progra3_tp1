package progra3_tp1.utilidades.excepciones;

/**
 * excepcion personalizada para caso de Fila o Columna inexistente.
 * @author santiago
 *
 */
public class SinParDeNumerosExcepcion extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public SinParDeNumerosExcepcion(String mensaje) {
		super(mensaje);
	}

}
