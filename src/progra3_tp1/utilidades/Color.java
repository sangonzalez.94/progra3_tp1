package progra3_tp1.utilidades;

public class Color {
	public static final String COLOR_0 = "#ffffff";
	public static final String COLOR_2 ="#e3f2fd";
	public static final String COLOR_4 ="#bbdefb";
	public static final String COLOR_8 ="#90caf9";
	public static final String COLOR_16 ="#82b1ff";
	public static final String COLOR_32 ="#64b5f6";
	public static final String COLOR_64 ="#42a5f5";
	public static final String COLOR_128 ="#2196f3";
	public static final String COLOR_256 ="#1e88e5";
	public static final String COLOR_512 ="#1976d2";
	public static final String COLOR_1024 ="#1565c0";
	public static final String COLOR_2048 ="#0d47a1";
}
