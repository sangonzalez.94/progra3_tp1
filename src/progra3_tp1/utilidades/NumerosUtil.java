package progra3_tp1.utilidades;

/**
 * Clase con utilidades con numeros.
 * @author santiago
 *
 */
public class NumerosUtil {
	
	/**
	 * Devuelve un numero al azar entre un numero minimo -inclusive- y un numero maximo - inclusive-
	 * con la opción de devolver solo números pares. 
	 * @param numeroMinimo minimo tolerado
	 * @param numeroMaximo máximo tolerado
	 * @param par condicion de ser o no par.
	 * @return el numero al azar.
	 */
	public static Integer numeroAlAzar (int numeroMinimo, int numeroMaximo, boolean par) {
		if (par) {
			boolean parEncontrado = false;
			int numero = 0;
			while (!parEncontrado) {
				numero = numeroAlAzar (numeroMinimo, numeroMaximo);
				if(numero%2 ==0) {
					parEncontrado = true;
				}
			}
			return numero;
		}else {
			return numeroAlAzar (numeroMinimo, numeroMaximo);
		}
	}
	

	/**
	 * Devuelve un numero al azar entre un numero minimo -inclusive- y un numero maximo - inclusive-
	 * @param numeroMinimo minimo tolerado
	 * @param numeroMaximo máximo tolerado
	 * @return el numero al azar.
	 */
	public static Integer numeroAlAzar (int numeroMinimo, int numeroMaximo) {
		int valorEntero = (int) Math.floor(Math.random()*(numeroMaximo-numeroMinimo+1)+numeroMinimo);
		return valorEntero;
	}
	
	/**
	 * Devuelve si dos o más números son iguales entre si.
	 * @param integers
	 * @return true si son iguales. false si no lo son.
	 */
	public static boolean sonIguales(Integer ...integers ) {
		boolean sonIguales = true;
		for (int i = 1; i < integers.length; i++) {
			sonIguales = sonIguales && (integers[i-1]==integers[i]); 
		}
		return sonIguales;
	}
	
	/**
	 * devuelve si dos números son iguales.
	 * @param a primer numero a comparar.
	 * @param b segundo numero a comparar
	 * @return true si ambos numeros son iguales, false en caso contrario.
	 */
	public static boolean sonIguales(int a, int b) {
		return a == b;
	}
}
