package progra3_tp1.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import progra3_tp1.modelos.Tablero;
import progra3_tp1.utilidades.excepciones.SinParDeNumerosExcepcion;

public class TableroTest {
	Tablero tablero;
	int[][] resultado;
	@Test
	public void accionDerechaSinSumarNumeros() throws SinParDeNumerosExcepcion {
		cargarTableroCasoAccionDerechaSinSumarNumeros();
		ejecutarAccionDerechaSinSumarNumeros();
		verificarPruebaAccionDerechaSinSumarNumeros();
	}
	
	@Test
	public void accionDerechaSumandoSoloDosNumeros()throws SinParDeNumerosExcepcion  {
		cargarTableroCasoAccionDerechaSumandoSoloDosNumeros();
		ejecutarAccionDerechaSumandoSoloDosNumeros();
		verificarPruebaAccionDerechaSumandoSoloDosNumeros();
	}
	
	@Test
	public void accionDerechaSumandoSoloDosNumerosConUnTerceNumeroDistintoAlPrincipio()throws SinParDeNumerosExcepcion  {
		cargarTableroCasoAccionDerechaSumandoSoloDosNumerosConUnTercerNumeroDistinto();
		ejecutarAccionDerechaSumandoSoloDosNumerosConTerceroDistinto();
		verificarPruebaAccionDerechaSumandoSoloDosNumerosTerceroDistinto();
	}
	
	@Test
	public void accionDerechaSumandoSoloDosNumerosConUnTerceNumeroDistintoAlFinal() throws SinParDeNumerosExcepcion {
		cargarTableroCasoAccionDerechaSumandoSoloDosNumerosConUnTercerNumeroDistintoAlFinal();
		ejecutarAccionDerechaSumandoSoloDosNumerosConTerceroDistintoAlFinal();
		verificarPruebaAccionDerechaSumandoSoloDosNumerosTerceroDistintoAlFinal();
	}
	
	@Test
	public void accionDerechaSumandoConCuatroNumerosIgualesEnLaMismaFila()throws SinParDeNumerosExcepcion  {
		cargarTableroCasoAccionDerechaTodosNumerosIgualesEnLaMismaFila();
		ejecutarAccionDerechaTodosNumerosIgualesEnLaMismaFila();
		verificarPruebaAccionDerechaTodosNumerosIgualesEnLaMismaFila();
	}
	
	@Test
	public void accionDerechaSumandoConCuatroNumerosDistintos() throws SinParDeNumerosExcepcion {
		cargarTableroCasoAccionDerechaTodosNumerosDistintosEnLaMismaFila();
		ejecutarAccionDerechaTodosNumerosDistintosEnLaMismaFila();
		verificarPruebaAccionDerechaTodosNumerosDistintosEnLaMismaFila();
	}

	@Test
	public void accionIzquierdaSinSumarNumeros()throws SinParDeNumerosExcepcion  {
		cargarTableroCasoAccionIzquierdaSinSumarNumeros();
		ejecutarAccionIzquierdaSinSumarNumeros();
		verificarPruebaAccionIzquierdaSinSumarNumeros();
	}
	
	@Test
	public void accionIzquierdaSumandoSoloDosNumeros()throws SinParDeNumerosExcepcion  {
		cargarTableroCasoAccionIzquierdaSumandoSoloDosNumeros();
		ejecutarAccionIzquierdaSumandoSoloDosNumeros();
		verificarPruebaAccionIzquierdaSumandoSoloDosNumeros();
	}
	
	@Test
	public void accionIzquierdaSumandoSoloDosNumerosConUnTerceNumeroDistintoAlPrincipio()throws SinParDeNumerosExcepcion  {
		cargarTableroCasoAccionIzquierdaSumandoSoloDosNumerosConUnTercerNumeroDistintoAlPrincipio();
		ejecutarAccionIzquierdaSumandoSoloDosNumerosConTerceroDistintoAlPrincipio();
		verificarPruebaAccionIzquierdaSumandoSoloDosNumerosTerceroDistintoAlPrincipio();
	}
	
	@Test
	public void accionIzquierdaSumandoSoloDosNumerosConUnTerceNumeroDistintoAlFinal()throws SinParDeNumerosExcepcion  {
		cargarTableroCasoAccionIzquierdaSumandoSoloDosNumerosConUnTercerNumeroDistintoAlFinal();
		ejecutarAccionIzquierdaSumandoSoloDosNumerosConTerceroDistintoAlFinal();
		verificarPruebaAccionIzquierdaSumandoSoloDosNumerosTerceroDistintoAlFinal();
	}
	
	@Test
	public void accionIzquierdaSumandoConCuatroNumerosIgualesEnLaMismaFila()throws SinParDeNumerosExcepcion  {
		cargarTableroCasoAccionIzquierdaTodosNumerosIgualesEnLaMismaFila();
		ejecutarAccionIzquierdaTodosNumerosIgualesEnLaMismaFila();
		verificarPruebaAccionIzquierdaTodosNumerosIgualesEnLaMismaFila();
	}
	
	@Test
	public void accionIzquierdaSumandoConCuatroNumerosDistintos()throws SinParDeNumerosExcepcion  {
		cargarTableroCasoAccionIzquierdaTodosNumerosDistintosEnLaMismaFila();
		ejecutarAccionIzquierdaTodosNumerosDistintosEnLaMismaFila();
		verificarPruebaAccionIzquierdaTodosNumerosDistintosEnLaMismaFila();
	}
	
	@Test
	public void accionAbajoSinSumarNumeros()throws SinParDeNumerosExcepcion  {
		cargarTableroCasoAccionAbajoSinSumarNumeros();
		ejecutarAccionAbajoSinSumarNumeros();
		verificarPruebaAccionAbajoSinSumarNumeros();
	}
	
	@Test
	public void accionAbajoSumandoSoloDosNumeros()throws SinParDeNumerosExcepcion  {
		cargarTableroCasoAccionAbajoSumandoSoloDosNumeros();
		ejecutarAccionAbajoSumandoSoloDosNumeros();
		verificarPruebaAccionAbajoSumandoSoloDosNumeros();
	}
	
	@Test
	public void accionAbajoSumandoSoloDosNumerosConUnTerceNumeroDistintoAlPrincipio() throws SinParDeNumerosExcepcion {
		cargarTableroCasoAccionAbajoSumandoSoloDosNumerosConUnTercerNumeroDistinto();
		ejecutarAccionAbajoSumandoSoloDosNumerosConTerceroDistinto();
		verificarPruebaAccionAbajoSumandoSoloDosNumerosTerceroDistinto();
	}
	
	@Test
	public void accionAbajoSumandoSoloDosNumerosConUnTerceNumeroDistintoAlFinal() throws SinParDeNumerosExcepcion {
		cargarTableroCasoAccionAbajoSumandoSoloDosNumerosConUnTercerNumeroDistintoAlFinal();
		ejecutarAccionAbajoSumandoSoloDosNumerosConTerceroDistintoAlFinal();
		verificarPruebaAccionAbajoSumandoSoloDosNumerosTerceroDistintoAlFinal();
	}
	
	@Test
	public void accionAbajoSumandoConCuatroNumerosIgualesEnLaMismaColumna() throws SinParDeNumerosExcepcion {
		cargarTableroCasoAccionAbajoTodosNumerosIgualesEnLaMismaColumna();
		ejecutarAccionAbajoTodosNumerosIgualesEnLaMismaColumna();
		verificarPruebaAccionAbajoTodosNumerosIgualesEnLaMismaColumna();
	}
	
	@Test
	public void accionAbajoSumandoConCuatroNumerosDistintos() throws SinParDeNumerosExcepcion {
		cargarTableroCasoAccionAbajoTodosNumerosDistintosEnLaMismaColumna();
		ejecutarAccionAbajoTodosNumerosDistintosEnLaMismaColumna();
		verificarPruebaAccionAbajoTodosNumerosDistintosEnLaMismaColumna();
	}

	@Test
	public void accionArribaSinSumarNumeros() throws SinParDeNumerosExcepcion {
		cargarTableroCasoAccionArribaSinSumarNumeros();
		ejecutarAccionArribaSinSumarNumeros();
		verificarPruebaAccionArribaSinSumarNumeros();
	}
	
	@Test
	public void accionArribaSumandoSoloDosNumeros()throws SinParDeNumerosExcepcion  {
		cargarTableroCasoAccionArribaSumandoSoloDosNumeros();
		ejecutarAccionArribaSumandoSoloDosNumeros();
		verificarPruebaAccionArribaSumandoSoloDosNumeros();
	}
	
	@Test
	public void accionArribaSumandoSoloDosNumerosConUnTerceNumeroDistintoAlPrincipio() throws SinParDeNumerosExcepcion {
		cargarTableroCasoAccionArribaSumandoSoloDosNumerosConUnTercerNumeroDistinto();
		ejecutarAccionArribaSumandoSoloDosNumerosConTerceroDistinto();
		verificarPruebaAccionArribaSumandoSoloDosNumerosTerceroDistinto();
	}
	
	@Test
	public void accionArribaSumandoSoloDosNumerosConUnTerceNumeroDistintoAlFinal() throws SinParDeNumerosExcepcion {
		cargarTableroCasoAccionArribaSumandoSoloDosNumerosConUnTercerNumeroDistintoAlFinal();
		ejecutarAccionArribaSumandoSoloDosNumerosConTerceroDistintoAlFinal();
		verificarPruebaAccionArribaSumandoSoloDosNumerosTerceroDistintoAlFinal();
	}
	
	@Test
	public void accionArribaSumandoConCuatroNumerosIgualesEnLaMismaColumna() throws SinParDeNumerosExcepcion  {
		cargarTableroCasoAccionArribaTodosNumerosIgualesEnLaMismaColumna();
		ejecutarAccionArribaTodosNumerosIgualesEnLaMismaColumna();
		verificarPruebaAccionArribaTodosNumerosIgualesEnLaMismaColumna();
	}
	
	@Test
	public void accionArribaSumandoConCuatroNumerosDistintos() throws SinParDeNumerosExcepcion  {
		cargarTableroCasoAccionArribaTodosNumerosDistintosEnLaMismaColumna();
		ejecutarAccionArribaTodosNumerosDistintosEnLaMismaColumna();
		verificarPruebaAccionArribaTodosNumerosDistintosEnLaMismaColumna();
	}
	
	@Test
	public void accionDerechaProbarSumaDe8_16_resultado32() throws SinParDeNumerosExcepcion {
		cargarTableroConFilas8888();
		ejecutarAccionDerecha();
		ejecutarAccionDerecha();
		verificarResultado32PorSumar8888();
	}
	
	@Test
	public void accionDerechaProbarSumaDe16_32_resultado64() throws SinParDeNumerosExcepcion {
		cargarTableroConFilas16();
		ejecutarAccionDerecha();
		ejecutarAccionDerecha();
		verificarResultado64PorSumar16();
	}

	/*inicio Caso 1 de pruebas*/
	private void cargarTableroCasoAccionDerechaSinSumarNumeros() {
		tablero = new Tablero(obtenerTableroConDosNumerosEnDistintasFilas());
	}
	
	private void ejecutarAccionDerechaSinSumarNumeros() throws SinParDeNumerosExcepcion {
		resultado = tablero.accionDerecha();
	}
	
	private void verificarPruebaAccionDerechaSinSumarNumeros() {
		
		assertEquals("El numero debe ser 4",4,resultado[0][3]);
		assertEquals("El numero debe ser 2",2,resultado[3][3]);
	}

	private int[][] obtenerTableroConDosNumerosEnDistintasFilas() {
		int[][] matriz = new int [4][4];
		matriz[0][2] = 4;
		matriz[3][1] = 2;
		return matriz;
	}
	
	/*Fin de caso 1 de pruebas*/
	
	/*inicio Caso 2 de pruebas*/
	private void cargarTableroCasoAccionDerechaSumandoSoloDosNumeros() {
		tablero = new Tablero(obtenerTableroConDosNumerosIgualesEnLaMismaFila());
	}
	
	private void ejecutarAccionDerechaSumandoSoloDosNumeros() throws SinParDeNumerosExcepcion {
		resultado = tablero.accionDerecha();
	}
	
	private void verificarPruebaAccionDerechaSumandoSoloDosNumeros() {
		assertEquals("El resultado Esperado debe ser 8", 8, resultado[0][3]);
	}

	private int[][] obtenerTableroConDosNumerosIgualesEnLaMismaFila() {
		int[][] matriz = new int [4][4];
		matriz[0][1] = 4;
		matriz[0][2] = 4;
		return matriz;
	}
	
	/*Fin de caso 2 de pruebas*/
	
	/*inicio Caso 3 de pruebas*/
	private void cargarTableroCasoAccionDerechaSumandoSoloDosNumerosConUnTercerNumeroDistinto() {
		tablero = new Tablero(obtenerTableroConDosNumerosIgualesEnLaMismaFilaYUnTerceroDistinto());
	}
	
	private void ejecutarAccionDerechaSumandoSoloDosNumerosConTerceroDistinto() throws SinParDeNumerosExcepcion {
		resultado = tablero.accionDerecha();
	}
	
	private void verificarPruebaAccionDerechaSumandoSoloDosNumerosTerceroDistinto() {
		assertEquals("El resultado debe ser 4. Ya que se lo movio desde la primer columna", 4, resultado[0][2]);
		assertEquals("El resultado debe ser 4. Es la sumatoria de dos numeros 2",4, resultado[0][3]);
	}

	private int[][] obtenerTableroConDosNumerosIgualesEnLaMismaFilaYUnTerceroDistinto() {
		int[][] matriz = new int [4][4];
		matriz[0][0] = 4;
		matriz[0][1] = 2;
		matriz[0][2] = 2;
		return matriz;
	}
	
	/*Fin de caso 3 de pruebas*/
	
	/*inicio Caso 4 de pruebas*/
	private void cargarTableroCasoAccionDerechaSumandoSoloDosNumerosConUnTercerNumeroDistintoAlFinal() {
		tablero = new Tablero(obtenerTableroConDosNumerosIgualesEnLaMismaFilaYUnTerceroDistintoAlFinal());
	}
	
	private void ejecutarAccionDerechaSumandoSoloDosNumerosConTerceroDistintoAlFinal() throws SinParDeNumerosExcepcion {
		resultado = tablero.accionDerecha();
	}
	
	private void verificarPruebaAccionDerechaSumandoSoloDosNumerosTerceroDistintoAlFinal() {
		assertEquals("El resultado debe ser 4. Es la sumatoria de las dos primeras columnas", 4, resultado[0][2]);
		assertEquals("El resultado debe ser 4. Es la tercer columna movida al final",4, resultado[0][3]);
	}

	private int[][] obtenerTableroConDosNumerosIgualesEnLaMismaFilaYUnTerceroDistintoAlFinal() {
		int[][] matriz = new int [4][4];
		matriz[0][0] = 2;
		matriz[0][1] = 2;
		matriz[0][2] = 4;
		return matriz;
	}
	
	/*Fin de caso 4 de pruebas*/
	
	
	/*inicio Caso 5 de pruebas*/
	private void cargarTableroCasoAccionDerechaTodosNumerosIgualesEnLaMismaFila() {
		tablero = new Tablero(obtenerTableroConTodosNumerosIgualesEnLaMismaFila());
	}
	
	private void ejecutarAccionDerechaTodosNumerosIgualesEnLaMismaFila()throws SinParDeNumerosExcepcion   {
		resultado = tablero.accionDerecha();
	}
	
	private void verificarPruebaAccionDerechaTodosNumerosIgualesEnLaMismaFila() {
		assertEquals("El resultado debe ser 4. Es la sumatoria de las dos primeras columnas", 4, resultado[0][2]);
		assertEquals("El resultado debe ser 4. Es la tercer columna movida al final",4, resultado[0][3]);
	}

	private int[][] obtenerTableroConTodosNumerosIgualesEnLaMismaFila() {
		int[][] matriz = new int [4][4];
		matriz[0][0] = 2;
		matriz[0][1] = 2;
		matriz[0][2] = 2;
		matriz[0][3] = 2;
		return matriz;
	}
	
	/*inicio Caso 6 de pruebas*/
	private void cargarTableroCasoAccionDerechaTodosNumerosDistintosEnLaMismaFila() {
		tablero = new Tablero(obtenerTableroConTodosNumerosDistintosEnLaMismaFila());
	}
	
	private void ejecutarAccionDerechaTodosNumerosDistintosEnLaMismaFila() throws SinParDeNumerosExcepcion {
		resultado = tablero.accionDerecha();
	}
	
	private void verificarPruebaAccionDerechaTodosNumerosDistintosEnLaMismaFila() {
		System.out.println("/"+resultado[0][0]+"/"+resultado[0][1]+"/"+resultado[0][2]+"/"+resultado[0][3]);
		assertEquals("El resultado debe ser 2.", 2, resultado[0][0]);
		assertEquals("El resultado debe ser 4.",4, resultado[0][1]);
		assertEquals("El resultado debe ser 8.", 8, resultado[0][2]);
		assertEquals("El resultado debe ser 16.",16, resultado[0][3]);
	}

	private int[][] obtenerTableroConTodosNumerosDistintosEnLaMismaFila() {
		int[][] matriz = new int [4][4];
		matriz[0][0] = 2;
		matriz[0][1] = 4;
		matriz[0][2] = 8;
		matriz[0][3] = 16;
		return matriz;
	}
	
	
	/*Fin de caso 6 de pruebas*/
	
	
	/*inicio Caso 7 de pruebas*/
	private void cargarTableroCasoAccionIzquierdaSinSumarNumeros() {
		tablero = new Tablero(obtenerTableroConDosNumerosEnDistintasFilasAccionIzquierda());
	}
	
	private void ejecutarAccionIzquierdaSinSumarNumeros() throws SinParDeNumerosExcepcion {
		resultado = tablero.accionIzquierda();
	}
	
	private void verificarPruebaAccionIzquierdaSinSumarNumeros() {
		
		assertEquals("El numero debe ser 4",4,resultado[0][0]);
		assertEquals("El numero debe ser 2",2,resultado[3][0]);
	}

	private int[][] obtenerTableroConDosNumerosEnDistintasFilasAccionIzquierda() {
		int[][] matriz = new int [4][4];
		matriz[0][3] = 4;
		matriz[3][2] = 2;
		return matriz;
	}
	
	/*Fin de caso 7 de pruebas*/
	
	/*inicio Caso 8 de pruebas*/
	private void cargarTableroCasoAccionIzquierdaSumandoSoloDosNumeros() {
		tablero = new Tablero(obtenerTableroConDosNumerosIgualesEnLaMismaFilaHaciaIzquierda());
	}
	
	private void ejecutarAccionIzquierdaSumandoSoloDosNumeros() throws SinParDeNumerosExcepcion {
		resultado = tablero.accionIzquierda();
	}
	
	private void verificarPruebaAccionIzquierdaSumandoSoloDosNumeros() {
		assertEquals("El resultado Esperado debe ser 8", 8, resultado[0][0]);
	}

	private int[][] obtenerTableroConDosNumerosIgualesEnLaMismaFilaHaciaIzquierda() {
		int[][] matriz = new int [4][4];
		matriz[0][1] = 4;
		matriz[0][2] = 4;
		return matriz;
	}
	
	/*Fin de caso 8 de pruebas*/
	
	/*inicio Caso 9 de pruebas*/
	private void cargarTableroCasoAccionIzquierdaSumandoSoloDosNumerosConUnTercerNumeroDistintoAlPrincipio() {
		tablero = new Tablero(obtenerTableroConDosNumerosIgualesEnLaMismaFilaYUnTerceroDistintoAlPrincipioIzquierda());
	}
	
	private void ejecutarAccionIzquierdaSumandoSoloDosNumerosConTerceroDistintoAlPrincipio() throws SinParDeNumerosExcepcion  {
		resultado = tablero.accionIzquierda();
	}
	
	private void verificarPruebaAccionIzquierdaSumandoSoloDosNumerosTerceroDistintoAlPrincipio() {
		assertEquals("El resultado debe ser 4. Ya que se lo movio desde la ultima columna", 4, resultado[0][0]);
		assertEquals("El resultado debe ser 4. Es la sumatoria de dos numeros 2",4, resultado[0][1]);
	}

	private int[][] obtenerTableroConDosNumerosIgualesEnLaMismaFilaYUnTerceroDistintoAlPrincipioIzquierda() {
		int[][] matriz = new int [4][4];
		matriz[0][1] = 4;
		matriz[0][2] = 2;
		matriz[0][3] = 2;
		return matriz;
	}
	
	/*Fin de caso 9 de pruebas*/
	
	/*inicio Caso 10 de pruebas*/
	private void cargarTableroCasoAccionIzquierdaSumandoSoloDosNumerosConUnTercerNumeroDistintoAlFinal() {
		tablero = new Tablero(obtenerTableroConDosNumerosIgualesEnLaMismaFilaYUnTerceroDistintoAlFinalIzquierda());
	}
	
	private void ejecutarAccionIzquierdaSumandoSoloDosNumerosConTerceroDistintoAlFinal()throws SinParDeNumerosExcepcion  {
		resultado = tablero.accionIzquierda();
	}
	
	private void verificarPruebaAccionIzquierdaSumandoSoloDosNumerosTerceroDistintoAlFinal() {
		assertEquals("El resultado debe ser 4. Es la sumatoria de las dos primeras columnas", 4, resultado[0][1]);
		assertEquals("El resultado debe ser 4. Es la tercer columna movida al final",4, resultado[0][0]);
	}

	private int[][] obtenerTableroConDosNumerosIgualesEnLaMismaFilaYUnTerceroDistintoAlFinalIzquierda() {
		int[][] matriz = new int [4][4];
		matriz[0][1] = 2;
		matriz[0][2] = 2;
		matriz[0][3] = 4;
		return matriz;
	}
	
	/*Fin de caso 10 de pruebas*/
	
	/*inicio Caso 5 de pruebas*/
	private void cargarTableroCasoAccionIzquierdaTodosNumerosIgualesEnLaMismaFila() {
		tablero = new Tablero(obtenerTableroConTodosNumerosIgualesEnLaMismaFila());
	}
	
	private void ejecutarAccionIzquierdaTodosNumerosIgualesEnLaMismaFila() throws SinParDeNumerosExcepcion {
		resultado = tablero.accionIzquierda();
	}
	
	private void verificarPruebaAccionIzquierdaTodosNumerosIgualesEnLaMismaFila() {
		assertEquals("El resultado debe ser 4. Es la sumatoria de las dos primeras columnas", 4, resultado[0][0]);
		assertEquals("El resultado debe ser 4. Es la tercer columna movida al final",4, resultado[0][1]);
	}

	
	/*inicio Caso 6 de pruebas*/
	private void cargarTableroCasoAccionIzquierdaTodosNumerosDistintosEnLaMismaFila() {
		tablero = new Tablero(obtenerTableroConTodosNumerosDistintosEnLaMismaFila());
	}
	
	private void ejecutarAccionIzquierdaTodosNumerosDistintosEnLaMismaFila()throws SinParDeNumerosExcepcion  {
		resultado = tablero.accionIzquierda();
	}
	
	private void verificarPruebaAccionIzquierdaTodosNumerosDistintosEnLaMismaFila() {
		assertEquals("El resultado debe ser 2.", 2, resultado[0][0]);
		assertEquals("El resultado debe ser 4.",4, resultado[0][1]);
		assertEquals("El resultado debe ser 8.", 8, resultado[0][2]);
		assertEquals("El resultado debe ser 16.",16, resultado[0][3]);
	}
	
	/*inicio Caso 1 de pruebas*/
	private void cargarTableroCasoAccionAbajoSinSumarNumeros() {
		tablero = new Tablero(obtenerTableroConDosNumerosEnDistintasFilasAbajo());
	}
	
	private void ejecutarAccionAbajoSinSumarNumeros() throws SinParDeNumerosExcepcion {
		resultado = tablero.accionAbajo();
	}
	
	private void verificarPruebaAccionAbajoSinSumarNumeros() {
		
		assertEquals("El numero debe ser 4",4,resultado[3][2]);
		assertEquals("El numero debe ser 2",2,resultado[3][1]);
	}

	private int[][] obtenerTableroConDosNumerosEnDistintasFilasAbajo() {
		int[][] matriz = new int [4][4];
		matriz[0][2] = 4;
		matriz[3][1] = 2;
		return matriz;
	}
	
	/*inicio Caso 13 de pruebas*/
	private void cargarTableroCasoAccionAbajoSumandoSoloDosNumeros() {
		tablero = new Tablero(obtenerTableroConDosNumerosIgualesEnLaMismaColumnaAccionAbajo());
	}
	
	private void ejecutarAccionAbajoSumandoSoloDosNumeros() throws SinParDeNumerosExcepcion {
		resultado = tablero.accionAbajo();
	}
	
	private void verificarPruebaAccionAbajoSumandoSoloDosNumeros() {
		assertEquals("El resultado Esperado debe ser 8", 8, resultado[3][1]);
	}

	private int[][] obtenerTableroConDosNumerosIgualesEnLaMismaColumnaAccionAbajo() {
		int[][] matriz = new int [4][4];
		matriz[0][1] = 4;
		matriz[2][1] = 4;
		return matriz;
	}
	
	/*Fin de caso 13 de pruebas*/
	
	/*inicio Caso 3 de pruebas*/
	private void cargarTableroCasoAccionAbajoSumandoSoloDosNumerosConUnTercerNumeroDistinto() {
		tablero = new Tablero(obtenerTableroConDosNumerosIgualesEnLaMismaColumnaYUnTerceroDistinto());
	}
	
	private void ejecutarAccionAbajoSumandoSoloDosNumerosConTerceroDistinto() throws SinParDeNumerosExcepcion  {
		resultado = tablero.accionAbajo();
	}
	
	private void verificarPruebaAccionAbajoSumandoSoloDosNumerosTerceroDistinto() {
		
		assertEquals("El resultado debe ser 4. Ya que se lo movio desde la primer columna", 4, resultado[3][0]);
		assertEquals("El resultado debe ser 4. Es la sumatoria de dos numeros 2",4, resultado[3][0]);
	}

	private int[][] obtenerTableroConDosNumerosIgualesEnLaMismaColumnaYUnTerceroDistinto() {
		int[][] matriz = new int [4][4];
		matriz[0][0] = 4;
		matriz[1][0] = 2;
		matriz[2][0] = 2;
			return matriz;
	}
	
	/*Fin de caso 3 de pruebas*/
	
	/*inicio Caso 4 de pruebas*/
	private void cargarTableroCasoAccionAbajoSumandoSoloDosNumerosConUnTercerNumeroDistintoAlFinal() {
		tablero = new Tablero(obtenerTableroConDosNumerosIgualesEnLaMismaColumnaYUnTerceroDistintoAlFinal());
	}
	
	private void ejecutarAccionAbajoSumandoSoloDosNumerosConTerceroDistintoAlFinal() throws SinParDeNumerosExcepcion {
		resultado = tablero.accionAbajo();
	}
	
	private void verificarPruebaAccionAbajoSumandoSoloDosNumerosTerceroDistintoAlFinal() {
		assertEquals("El resultado debe ser 4. Es la sumatoria de las dos primeras columnas", 4, resultado[2][0]);
		assertEquals("El resultado debe ser 4. Es la tercer columna movida al final",4, resultado[3][0]);
	}

	private int[][] obtenerTableroConDosNumerosIgualesEnLaMismaColumnaYUnTerceroDistintoAlFinal() {
		int[][] matriz = new int [4][4];
		matriz[0][0] = 2;
		matriz[1][0] = 2;
		matriz[2][0] = 4;
		return matriz;
	}
	
	/*Fin de caso 4 de pruebas*/
	
	
	/*inicio Caso 5 de pruebas*/
	private void cargarTableroCasoAccionAbajoTodosNumerosIgualesEnLaMismaColumna() {
		tablero = new Tablero(obtenerTableroConTodosNumerosIgualesEnLaMismaColumna());
	}
	
	private void ejecutarAccionAbajoTodosNumerosIgualesEnLaMismaColumna()throws SinParDeNumerosExcepcion  {
		resultado = tablero.accionAbajo();
	}
	
	private void verificarPruebaAccionAbajoTodosNumerosIgualesEnLaMismaColumna() {
			assertEquals("El resultado debe ser 4. Es la sumatoria de las dos primeras columnas", 4, resultado[3][0]);
		assertEquals("El resultado debe ser 4. Es la tercer columna movida al final",4, resultado[2][0]);
	}

	private int[][] obtenerTableroConTodosNumerosIgualesEnLaMismaColumna() {
		int[][] matriz = new int [4][4];
		matriz[0][0] = 2;
		matriz[1][0] = 2;
		matriz[2][0] = 2;
		matriz[3][0] = 2;
			return matriz;
	}
	
	/*inicio Caso 6 de pruebas*/
	private void cargarTableroCasoAccionAbajoTodosNumerosDistintosEnLaMismaColumna() {
		tablero = new Tablero(obtenerTableroConTodosNumerosDistintosEnLaMismaColumna());
	}
	
	private void ejecutarAccionAbajoTodosNumerosDistintosEnLaMismaColumna() throws SinParDeNumerosExcepcion  {
		resultado = tablero.accionAbajo();
	}
	
	private void verificarPruebaAccionAbajoTodosNumerosDistintosEnLaMismaColumna() {
		assertEquals("El resultado debe ser 2.", 2, resultado[0][0]);
		assertEquals("El resultado debe ser 4.",4, resultado[1][0]);
		assertEquals("El resultado debe ser 8.", 8, resultado[2][0]);
		assertEquals("El resultado debe ser 16.",16, resultado[3][0]);
	}

	private int[][] obtenerTableroConTodosNumerosDistintosEnLaMismaColumna() {
		int[][] matriz = new int [4][4];
		matriz[0][0] = 2;
		matriz[1][0] = 4;
		matriz[2][0] = 8;
		matriz[3][0] = 16;
		return matriz;
	}
	
	
	/*Fin de caso 6 de pruebas*/

	
	/*inicio Caso 1 de pruebas*/
	private void cargarTableroCasoAccionArribaSinSumarNumeros() {
		tablero = new Tablero(obtenerTableroConDosNumerosEnDistintasFilasArriba());
	}
	
	private void ejecutarAccionArribaSinSumarNumeros() throws SinParDeNumerosExcepcion {
		resultado = tablero.accionArriba();
	}
	
	private void verificarPruebaAccionArribaSinSumarNumeros() {
		
		assertEquals("El numero debe ser 4",4,resultado[0][2]);
		assertEquals("El numero debe ser 2",2,resultado[0][1]);
	}

	private int[][] obtenerTableroConDosNumerosEnDistintasFilasArriba() {
		int[][] matriz = new int [4][4];
		matriz[0][2] = 4;
		matriz[3][1] = 2;
		return matriz;
	}
	
	/*inicio Caso 13 de pruebas*/
	private void cargarTableroCasoAccionArribaSumandoSoloDosNumeros() {
		tablero = new Tablero(obtenerTableroConDosNumerosIgualesEnLaMismaColumnaAccionArriba());
	}
	
	private void ejecutarAccionArribaSumandoSoloDosNumeros() throws SinParDeNumerosExcepcion {
		resultado = tablero.accionArriba();
	}
	
	private void verificarPruebaAccionArribaSumandoSoloDosNumeros() {
		assertEquals("El resultado Esperado debe ser 8", 8, resultado[0][1]);
	}

	private int[][] obtenerTableroConDosNumerosIgualesEnLaMismaColumnaAccionArriba() {
		int[][] matriz = new int [4][4];
		matriz[0][1] = 4;
		matriz[2][1] = 4;
		return matriz;
	}
	
	/*Fin de caso 13 de pruebas*/
	
	/*inicio Caso 3 de pruebas*/
	private void cargarTableroCasoAccionArribaSumandoSoloDosNumerosConUnTercerNumeroDistinto() {
		tablero = new Tablero(obtenerTableroConDosNumerosIgualesEnLaMismaColumnaYUnTerceroDistinto());
	}
	
	private void ejecutarAccionArribaSumandoSoloDosNumerosConTerceroDistinto() throws SinParDeNumerosExcepcion {
		resultado = tablero.accionArriba();
	}
	
	private void verificarPruebaAccionArribaSumandoSoloDosNumerosTerceroDistinto() {
		
		assertEquals("El resultado debe ser 4. Ya que se lo movio desde la primer columna", 4, resultado[0][0]);
		assertEquals("El resultado debe ser 4. Es la sumatoria de dos numeros 2",4, resultado[1][0]);
	}


	
	/*Fin de caso 3 de pruebas*/
	
	/*inicio Caso 4 de pruebas*/
	private void cargarTableroCasoAccionArribaSumandoSoloDosNumerosConUnTercerNumeroDistintoAlFinal() {
		tablero = new Tablero(obtenerTableroConDosNumerosIgualesEnLaMismaColumnaYUnTerceroDistintoAlFinal());
	}
	
	private void ejecutarAccionArribaSumandoSoloDosNumerosConTerceroDistintoAlFinal() throws SinParDeNumerosExcepcion {
		resultado = tablero.accionArriba();
	}
	
	private void verificarPruebaAccionArribaSumandoSoloDosNumerosTerceroDistintoAlFinal() {
		assertEquals("El resultado debe ser 4. Es la sumatoria de las dos primeras columnas", 4, resultado[0][0]);
		assertEquals("El resultado debe ser 4. Es la tercer columna movida al final",4, resultado[1][0]);
	}
	
	/*Fin de caso 4 de pruebas*/
	
	
	/*inicio Caso 5 de pruebas*/
	private void cargarTableroCasoAccionArribaTodosNumerosIgualesEnLaMismaColumna() {
		tablero = new Tablero(obtenerTableroConTodosNumerosIgualesEnLaMismaColumna());
	}
	
	private void ejecutarAccionArribaTodosNumerosIgualesEnLaMismaColumna()throws SinParDeNumerosExcepcion  {
		resultado = tablero.accionArriba();
	}
	
	private void verificarPruebaAccionArribaTodosNumerosIgualesEnLaMismaColumna() {
			assertEquals("El resultado debe ser 4. Es la sumatoria de las dos primeras columnas", 4, resultado[1][0]);
		assertEquals("El resultado debe ser 4. Es la tercer columna movida al final",4, resultado[0][0]);
	}


	
	/*inicio Caso 6 de pruebas*/
	private void cargarTableroCasoAccionArribaTodosNumerosDistintosEnLaMismaColumna() {
		tablero = new Tablero(obtenerTableroConTodosNumerosDistintosEnLaMismaColumna());
	}
	
	private void ejecutarAccionArribaTodosNumerosDistintosEnLaMismaColumna() throws SinParDeNumerosExcepcion {
		resultado = tablero.accionArriba();
	}
	
	private void verificarPruebaAccionArribaTodosNumerosDistintosEnLaMismaColumna() {
		System.out.println("/"+resultado[0][0]+"/"+resultado[1][0]+"/"+resultado[2][0]+"/"+resultado[3][0]);
		assertEquals("El resultado debe ser 2.", 2, resultado[0][0]);
		assertEquals("El resultado debe ser 4.",4, resultado[1][0]);
		assertEquals("El resultado debe ser 8.", 8, resultado[2][0]);
		assertEquals("El resultado debe ser 16.",16, resultado[3][0]);
	}

	
	
	
	/*Fin de caso 6 de pruebas*/
	
	/*inicio Caso 6 de pruebas*/
	private void cargarTableroConFilas8888() {
		tablero = new Tablero(obtenerTablero8888());
	}
	
	private void verificarResultado32PorSumar8888() {
		System.out.println("/"+resultado[0][0]+"/"+resultado[0][1]+"/"+resultado[0][2]+"/"+resultado[0][3]);
		assertEquals("El resultado debe ser 32.", 32, resultado[0][3]);
	
	}
	private int[][] obtenerTablero8888() {
		int[][] matriz = new int [4][4];
		matriz[0][0] = 8;
		matriz[0][1] = 8;
		matriz[0][2] = 8;
		matriz[0][3] = 8;
		return matriz;
	}
	
	/*inicio Caso 6 de pruebas*/
	private void cargarTableroConFilas16() {
		tablero = new Tablero(obtenerTablero16());
	}
	
	private void verificarResultado64PorSumar16() {
		System.out.println("/"+resultado[0][0]+"/"+resultado[0][1]+"/"+resultado[0][2]+"/"+resultado[0][3]);
		assertEquals("El resultado debe ser 64.", 64, resultado[0][3]);
	
	}
	private int[][] obtenerTablero16() {
		int[][] matriz = new int [4][4];
		matriz[0][0] = 16;
		matriz[0][1] = 16;
		matriz[0][2] = 16;
		matriz[0][3] = 16;
		return matriz;
	}
	
	
	private void ejecutarAccionDerecha() throws SinParDeNumerosExcepcion {
		resultado = tablero.accionDerecha();
	}
}
