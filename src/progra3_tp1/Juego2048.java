package progra3_tp1;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ContainerListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import java.awt.Color;

import static progra3_tp1.utilidades.ConstantesUtil.*;
import static progra3_tp1.utilidades.Color.*;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.DefaultHighlighter.DefaultHighlightPainter;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import progra3_tp1.modelos.Tablero;
import progra3_tp1.utilidades.excepciones.SinParDeNumerosExcepcion;
import javax.swing.JLabel;

public class Juego2048 {
	private static final int CANTIDAD_FILAS = 4;
	private static final int CANTIDAD_COLUMNAS = 4;
	private JFrame frame;
	private JTextPane[][] matrizTablero;
	private Tablero tablero;
	private JLabel lblFinJuego;
	private int movimientosTableroLleno = 0;
	//private DefaultHighlightPainter redPainter = new DefaultHighlighter.DefaultHighlightPainter(Color);
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Juego2048 window = new Juego2048();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @throws BadLocationException 
	 */
	public Juego2048() throws BadLocationException, SinParDeNumerosExcepcion  {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws BadLocationException 
	 */
	private void initialize() throws BadLocationException,  SinParDeNumerosExcepcion  {
		frame = new JFrame();
		frame.setBounds(100, 100, 600, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		lblFinJuego = new JLabel("FIN DE JUEGO.");
		lblFinJuego.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 19));
		lblFinJuego.setBounds(362, 495, 168, 29);
		lblFinJuego.setVisible(false);
		frame.getContentPane().add(lblFinJuego);
		try {
			tablero = new Tablero(CANTIDAD_FILAS, CANTIDAD_COLUMNAS);
		} catch (SinParDeNumerosExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dibujarTablero();
		agregarTeclasListener();
		tableroModelToView();
	}

	private void dibujarTablero() throws BadLocationException {
		int posInicialX = 200;
		int posInicialY = 123;
		int lonCuadrado = 50;
		matrizTablero = new JTextPane[CANTIDAD_FILAS][CANTIDAD_COLUMNAS];
		for (int fila = 0; fila < CANTIDAD_FILAS; fila++) {
			for (int columna = 0; columna < CANTIDAD_COLUMNAS; columna++) {
				JTextPane textArea = new JTextPane();
				textArea.setEnabled(false);
				textArea.setBounds(posInicialX, posInicialY, lonCuadrado, lonCuadrado);
				Font font = new Font(FUENTE_DE_TEXTO, Font.BOLD, 15);
				textArea.setFont(font);
				SimpleAttributeSet as = new SimpleAttributeSet();
				StyleConstants.setBold(as, true);
				StyleConstants.setForeground(as, Color.GREEN);
				textArea.setCharacterAttributes(as, true);
				matrizTablero[fila][columna] = textArea;
				frame.getContentPane().add(textArea);
				posInicialX = posInicialX + lonCuadrado;
			}
			posInicialX = 200;
			posInicialY = posInicialY + lonCuadrado;
		}
	}

	private void agregarTeclasListener() throws SinParDeNumerosExcepcion {
		try {
		frame.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
			}
			@Override
			public void keyReleased(KeyEvent e) {
				switch (e.getKeyCode()) {
				case KeyEvent.VK_LEFT:
					try {
						accionIzquierda();
					} catch (SinParDeNumerosExcepcion e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					break;
				case KeyEvent.VK_RIGHT:
					try {
						accionDerecha();
					} catch (SinParDeNumerosExcepcion e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					break;
				case KeyEvent.VK_UP:
					try {
						accionArriba();
					} catch (SinParDeNumerosExcepcion e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					break;
				case KeyEvent.VK_DOWN:
					try {
						accionAbajo();
					} catch (SinParDeNumerosExcepcion e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					break;
				default:
					break;
				}

			}
			@Override
			public void keyPressed(KeyEvent e) {
			}
		});
		}catch(Exception e) {
			
		}
	}

	private void accionDerecha()throws SinParDeNumerosExcepcion  {
		tablero.accionDerecha();
		tableroModelToView();
	}

	private void accionIzquierda() throws SinParDeNumerosExcepcion {
		tablero.accionIzquierda();
		tableroModelToView();
	}

	private void accionArriba() throws SinParDeNumerosExcepcion {
		tablero.accionArriba();
		tableroModelToView();
	}

	private void accionAbajo() throws SinParDeNumerosExcepcion {
		tablero.accionAbajo();
		verificarTableroCompleto();
		tableroModelToView();
	}
	
	private void verificarTableroCompleto() {
		if(tablero.isTableroCompleto()) {
			movimientosTableroLleno++;
		}
		if(movimientosTableroLleno>3 && tablero.isTableroCompleto()) {
			lblFinJuego.setVisible(true);
		}
		if(movimientosTableroLleno<=3 && !tablero.isTableroCompleto()) {
			movimientosTableroLleno= 0;
		}
	}

	private void tableroModelToView() {
		int[][] matriz = tablero.getTablero();
		for (int fila = 0; fila < CANTIDAD_FILAS; fila++) {
			for (int columna = 0; columna < CANTIDAD_COLUMNAS; columna++) {
				int valorTablero = matriz[fila][columna];
				String valorTableroString = Integer.toString(valorTablero);
				switch(valorTablero) {
				case 0:
					actualizarCeldaTablero(fila, columna,"", COLOR_0);
					break;
				case 2:
					actualizarCeldaTablero(fila, columna,valorTableroString, COLOR_2);
					break;
				case 4:
					actualizarCeldaTablero(fila, columna,valorTableroString, COLOR_4);
					break;
				case 8:
					actualizarCeldaTablero(fila, columna,valorTableroString, COLOR_8);
					break;
				case 16:
					actualizarCeldaTablero(fila, columna,valorTableroString, COLOR_16);
					break;
				case 32:
					actualizarCeldaTablero(fila, columna,valorTableroString, COLOR_32);
					break;
				case 64:
					actualizarCeldaTablero(fila, columna,valorTableroString, COLOR_64);
					break;
				case 128:
					actualizarCeldaTablero(fila, columna,valorTableroString, COLOR_128);
					break;
				case 256:
					actualizarCeldaTablero(fila, columna,valorTableroString, COLOR_256);
					break;
				case 512:
					actualizarCeldaTablero(fila, columna,valorTableroString, COLOR_512);
					break;
				case 1024:
					actualizarCeldaTablero(fila, columna,valorTableroString, COLOR_1024);
					break;
				case 2048:
					actualizarCeldaTablero(fila, columna,valorTableroString, COLOR_2048);
					break;
				}
				
			}
		}
	}

	private void actualizarCeldaTablero(int fila, int columna, String valor, String color) {
		matrizTablero[fila][columna].setText(valor);
		matrizTablero[fila][columna].setBackground(Color.decode(color));
	
	}
}
